//
//  LibraryViewController.swift
//  MusicApp
//
//  Created by Дмитрий Осипенко on 8.09.21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol LibraryDisplayLogic: class
{
  func displaySomething(viewModel: Library.LibraryModel.ViewModel)
}

class LibraryViewController: UIViewController, LibraryDisplayLogic
{
  var interactor: LibraryBusinessLogic?
  var router: (NSObjectProtocol & LibraryRoutingLogic & LibraryDataPassing)?
    @IBOutlet weak var tableView: UITableView!
    
  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Setup
  
  private func setup()
  {
    let viewController = self
    let interactor = LibraryInteractor()
    let presenter = LibraryPresenter()
    let router = LibraryRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
      super.viewDidLoad()
      doSomething()
      setupTableView()
    }
    
      private func setupTableView() {
          let nib = UINib(nibName: String(describing: LibraryCell.self), bundle: nil)
          tableView.register(nib, forCellReuseIdentifier: String(describing: LibraryCell.self))
          tableView.delegate = self
          tableView.dataSource = self
      }
    
    func doSomething()
    {
      let request = Library.LibraryModel.Request()
      interactor?.doSomething(request: request)
    }
    
    func displaySomething(viewModel: Library.LibraryModel.ViewModel)
    {
    }
  }

  //MARK: - UITableViewDelegate, UITableViewDataSource

  extension LibraryViewController: UITableViewDelegate, UITableViewDataSource {
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return 3
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: LibraryCell.self), for: indexPath)
          guard let libraryCell = cell as? LibraryCell else {return cell}
          return libraryCell
      }
      
      
  }

